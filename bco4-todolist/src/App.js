import logo from "./logo.svg";
import "./App.css";
import ToDoList from "./ToDoList";

function App() {
  return (
    <div className=" container py-5 App">
      <ToDoList />
    </div>
  );
}

export default App;
